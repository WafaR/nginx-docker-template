#!/bin/bash
# cesar@superadmin.es

set -e

dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
cd ${dir}
CWD=$(pwd)



i=10
while [ "$i" -ne 0 ]
do   
  if [ -f /var/www/html/wp-config.php ];then
  	i=0  
  else 
  	sleep 10
  	(( i-- ))
  fi  
done

# Runs the standard WordPress installation process.
docker run                                        \
  -u root                                         \
  --rm                                            \
  --volumes-from wordpress                        \
  --network container:wordpress                   \
  wordpress:cli                   				  \
  wp core install --url=${URL} --title="${HOSTNAME}" --admin_user=${HOSTNAME} --admin_email=${ADMIN_EMAIL} --admin_password=${ADMIN_PASSWORD} --skip-email --allow-root

# Refreshes the salts defined in the wp-config.php file.
docker run                                        \
  -u root                                         \
  --rm                                            \
  --volumes-from wordpress                        \
  --network container:wordpress                   \
  wordpress:cli                   				  \
  wp config shuffle-salts --allow-root  
